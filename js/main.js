$(function(){
  'use strict';

    // navbar sliding 
    $('.bars').click(function(){
        $('.nav-list').slideToggle();
    })

    // sticky navbar
    $(window).scroll(function(){
      var sc = $(this).scrollTop();
      if(sc > 50){
        $('header').addClass('sticky');
      }else{
        $('header').removeClass('sticky');
      }

      // count to plugin
      if(sc > 1774){
          $('.timer').countTo();
          // stop scroll event
          $(window).off('scroll');
          }

      // show scrollToTop icon
      if(sc > 500){
        $('.scroll-top').fadeIn();
      }else{
        $('.scroll-top').fadeOut();
      }

    })

    // buttons filtering 
    $('.btn-container button').click(function(){
      $(this).addClass('active-btn').siblings().removeClass('active-btn');

      var f = $(this).attr('id');
      if(f === 'all'){
        $('.imgs-container > div').fadeIn();
      } 
      else{
        $('.imgs-container > div').fadeOut();
        $('.imgs-container').contents().filter('.' + f).fadeIn();
      }         
    })


    // circle-progress plguin
    $('.progress.red').circleProgress({
      value: 0.75,
      size: 150,
      fill: { color: "#ff1e41" }
      });

      $('.progress.blue').circleProgress({
      value: 0.90,
      size: 150,
      fill: { color: "#dddddd" }
      });

      // owl carwsol >team section
      $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    })

    // choose
    $('.ul-list li').click(function(){
      $(this).addClass('active-btn').siblings().removeClass('active-btn');
      
        var myele = $(this).data('class');
        $('.content > div').hide();
        $('.content').contents().filter("."+myele).fadeIn();
    })
      
    // magnificat popup
    $('.pop').magnificPopup({
      type: 'iframe'
    });

    // slick section
    $('.slick').slick({
      prevArrow: false,
      nextArrow: false,
    });

    // scroll to top 
    $('.scroll-top').click(function(){
      $('html,body').animate({
        scrollTop: 0
      },500)
    })

    // smooth scroll

    $('.list a').click(function(){
      $('html,body').animate({
        scrollTop: $($(this).attr('href')).offset().top - 100
      })
    })


  });

//     $('html').niceScroll();